<?php
/**
 * Fancypics, Adds ux improvements to tidypics
 * Copyright (C) 2013 Juan José González
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

elgg_register_event_handler('init', 'system', 'fancypics_init');

/**
 * Plugin initialization
 */
function fancypics_init() {
  /*
   * This plugin depends entirely of tidypics 1.8
   */
  if (!elgg_plugin_exists('tidypics')){
    return FALSE;
  }

  elgg_register_page_handler('upload_photos', 'fancypics_page_handler');
  elgg_register_page_handler('photos', 'fancypics_album_page_handler');

  elgg_register_action('fancypics/upload', elgg_get_plugins_path() . 'fancypics/actions/upload.php');

  elgg_register_js('fancypics:dropzone', 'mod/fancypics/vendors/dropzone.js/dropzone.js');
  elgg_register_js('fancypics:dropzone.config', 'mod/fancypics/views/default/js/dropzone.config.js');
  elgg_register_js('fancypics:masonry', 'mod/fancypics/views/default/js/jquery.masonry.min.js');
  elgg_register_js('fancypics:infinitescroll', 'mod/fancypics/views/default/js/infinitescroll.min.js');
  elgg_register_js('fancypics:init', 'mod/fancypics/views/default/js/init.js');
  elgg_register_css('fancypics:dropzone', 'mod/fancypics/views/default/css/fancypics.css');
  elgg_register_css('fancypics:masonry', 'mod/fancypics/views/default/css/masonry.css');
}

/**
 * Redirect empty album view to upload photos to that album
 * @todo ux this way users can't delete albums
 */
function fancypics_album_page_handler($page) {
	elgg_load_js('fancypics:infinitescroll');
	elgg_load_js('fancypics:init');
	elgg_load_js('fancypics:masonry');
	elgg_load_css('fancypics:masonry');
	switch($page[0]){
	case "upload":
		/*
		 * redirect all album/* pages but album/{id}/{name}/basic (basic uploader)
		 */
		$is_basic_uploader = isset($page[3]) && $page[3] == 'basic';
		if ($is_basic_uploader){
			$page[2] = 'basic';
		}
		if (('upload' == $page[0]) || (!elgg_is_admin_logged_in() && $page[0] == 'album' && 0 == get_entity($page[1])->getSize())){
			$page[0] = 'upload';
			$page[2] = 'drop';
			fancypics_drop_uploader($page);
			return TRUE;
		}
		break;
	}
  // Page handler can continue as usual
  tidypics_page_handler($page);
}

function fancypics_drop_uploader($page){
  elgg_load_js('fancypics:dropzone');
  elgg_load_js('fancypics:dropzone.config');
  elgg_load_css('fancypics:dropzone');
  set_input('guid', $page[1]);

  if (elgg_get_plugin_setting('uploader', 'tidypics')) {
    $default_uploader = 'ajax';
  } else {
    $default_uploader = 'basic';
  }

  set_input('uploader', elgg_extract(2, $page, $default_uploader));
  require "pages/fancypics/upload.php";

  return true;
}

/**
 * An album is selected by default to upload photos,
 * it creates it if is not already created
 */
function fancypics_page_handler() {
  $user = elgg_get_logged_in_user_entity();
  if ($album_guid = fancypics_user_get_default_album_guid($user)) {
    forward("photos/upload/$album_guid");
  }
  else {
    fancypics_create_default_album($user);
  }
}

/**
 * Has this user a default album created?
 * @param $user ElggEntity logged in user's entity
 * @return int|false Returns default album's guid or false when does not exists
 * @todo default album is designed by name, it is desired to mark an album using metadata instead
 */
function fancypics_user_get_default_album_guid($user) {
  $db_prefix = get_config();
  $title = sanitize_string(elgg_echo("fancypics:default_album_name", array($user->name)));

  $conditions = array(
		      'type' => 'object',
		      'subtype' => 'album',
		      'container_guid' => $user->guid,
		      'limit' => 1,
		      'joins' => array("JOIN {$db_prefix}objects_entity oe on oe.guid = e.guid"),
		      'wheres' => array("oe.title = '$title'"),
		      );
  $album = elgg_get_entities($conditions);
  return $album[0]->guid ? $album[0]->guid : FALSE;
}

/**
 * Creates a default album using tidypics actions
 * @param $user ElggEntity logged in user's entity
 */
function fancypics_create_default_album($user) {
  set_input("title", elgg_echo("fancypics:default_album_name", array($user->name)));
  set_input("description", "");
  set_input("tags", null);
  set_input("access_id", 2); // Public
  set_input("container_guid", $user->guid);
  set_input("guid", 0);
  require(elgg_get_plugins_path() . "tidypics/actions/photos/album/save.php");
}