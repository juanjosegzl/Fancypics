<?php
/**
 * Fancypics, Adds ux improvements to tidypics
 * Copyright (C) 2013 Cash Costello, Juan José González
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA. 
 */

/*
 * This file is a copy of tidypics/pages/photos/image/upload.php
 * the only difference is this has a third uploader option: drop uploade
 */

gatekeeper();

$album_guid = (int) get_input('guid');
if ( ! $album_guid){
  forward();
}

$album = get_entity($album_guid);
if (! $album) {
  forward(REFERER);
}

if (!$album->getContainerEntity()->canWriteToContainer()) {
  forward(REFERER);
}

elgg_set_page_owner_guid($album->getcontainerGUID());
$owner = elgg_get_page_owner_entity();

// tidypics translation string
$title = elgg_echo('album:addpix');

// set up breadcrumbs                                                                                               
elgg_push_breadcrumb(elgg_echo('photos'), "photos/all");
elgg_push_breadcrumb($owner->name, "photos/owner/$owner->username");
elgg_push_breadcrumb($album->getTitle(), $album->getURL());
elgg_push_breadcrumb(elgg_echo('album:addpix'));

$uploader = get_input('uploader');
if ($uploader == 'basic') {
  $content = elgg_view('forms/photos/basic_upload', array('entity' => $album));
} else if ($uploader == 'drop') {
  $content = elgg_view('forms/fancypics/drop_upload', array('entity' => $album));
} else {
  elgg_load_js('swfobject');
  elgg_load_js('jquery.uploadify-tp');
  elgg_load_js('tidypics:uploading');
  $content = elgg_view('forms/photos/ajax_upload', array('entity' => $album));
}

$body = elgg_view_layout('content', array(
					  'content' => $content,
					  'title' => $title,
					  'filter' => '',
					  'sidebar' => elgg_view('photos/sidebar', array('page' => 'upload')),
					  ));

echo elgg_view_page($title, $body);