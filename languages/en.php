<?php
$english = array(
		 'fancypics:default_album_name' => "%s's pictures",
		 'fancypics:drop_here' => 'Click or drop your photos to upload them',
		 'fancypics:done' => 'Done',
		 );

add_translation("en", $english);