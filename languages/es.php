<?php
$spanish = array(
		 'fancypics:default_album_name' => "Fotos de %s",
		 'fancypics:drop_here' => 'Haz clic o arrastra tus fotos aquí para subirlas',
		 'fancypics:done' => '¡Listo!',
		 );

add_translation("es", $spanish);