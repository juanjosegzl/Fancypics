<?php
/**
 * Drop uploader form
 *
 */

$album = $vars['entity'];

$form_body = '<div class="default message"><span>'.elgg_echo('fancypics:drop_here').'</span></div>' . 
elgg_view('input/hidden', array('name' => 'guid', 'value' => $album->getGUID()));

echo '<p style="text-align:right"><a class="btn btn-info" href="/photos/album/'.$album->getGUID().'">'.elgg_echo('fancypics:done').'</a></p>';
echo elgg_view('input/form', array(
				   'body' => $form_body,
	'action' => 'action/fancypics/upload',
	'class' => 'dropzone',
	'id' => 'fancypics-dropzone',
));