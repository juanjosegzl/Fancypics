function masonry_look_like(){
    var $container = $('.tidypics-gallery');
    $container.imagesLoaded(function(){
        $container.masonry({
            itemSelector : '.elgg-item',
            columnWidth : function( containerWidth ) {
		return containerWidth / 3;
	    }
        });
    });
}

function countProperties(obj) {
    var count = 0;

    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            ++count;
    }

    return count;
}

function getURLParameter(url, parameter) {
    return decodeURI(
        (RegExp(parameter + '=' + '(.+?)(&|$)').exec(url)||[,null])[1]
    );
}



$(document).ready(function(){
    masonry_look_like();

//    $(".pagination ul li:last-child a").addClass('gallery-next-link');

    var $container = $('.tidypics-gallery');
    $container.parent().addClass("tidypics-gallery-container");
    $container.infinitescroll({
	navSelector  : ".pagination",
	nextSelector : ".pagination ul li:last-child a",
	itemSelector : ".elgg-item",
	bufferPx: 200,
	path: function(pageNumber) {
	    return $(".pagination ul li:last-child a").attr("href");
	},
	loading: {
	    finishedMsg: '',
	    msgText: 'Cargando más fotos',
	    selector: ".tidypics-gallery-container"
	},
	errorCallback: function(){
	    $(".tidypics-gallery-container").append("No hay más fotos en este álbum. <a href='/photos/world'>Ver más álbumes</a>");
	}
    },
			      function( newElements ) {
				  var $newElems = $(newElements).css({ opacity: 0 });
				  new_images_count = countProperties($newElems)-1;
				  next_href = $(".pagination ul li:last-child a").attr("href");
                                  old_offset = parseInt(getURLParameter(next_href, "offset"))
                                  new_offset = old_offset + new_images_count;
                                  new_next_href = next_href.replace("offset=" + old_offset, "offset=" + new_offset);
                                  $(".pagination ul li:last-child a").attr("href", new_next_href);
				  $newElems.imagesLoaded(function(){
				      $newElems.animate({ opacity: 1 });
				      $container.masonry( 'appended', $newElems);
				  });
			      }
			     );
});
